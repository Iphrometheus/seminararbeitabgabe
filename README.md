Anmerkung hier folgend ist meine Seminararbeit als Markdownfile. Die Seminararbeit ist aber auch als Pdf vorhanden.

###### Linus Hell Seminararbeit
# AWS DevOps

# Gliederung
1. Vorwort
2. DevOps
3. Grundbegriffe
4. Projekt Overview
    * Informationen zu geschriebenem Code
5. Fazit

## Vorwort

Leider konnte ich am Ende das Projekt nicht wie geplant abschließen. Jedoch konnte ich einige Tricks und Kniffe für zukünftige Verwendung mitnehmen.

Ich werde im Folgenden beschreiben, welche Schritte zu unternehmen sind,
um den aktuellen Stand der Seminararbeit nachzuvollziehen, beziehungsweise selber mit dem vorhandenen Code nachbauen zu können. Da ich ursprünglich nicht damit gerechnet hatte, meinen Code bereitzustellen,
ist die Dokumentation des Codes relativ schlecht. Sollten deshalb Fragen zum Code auftauchen, stelle ich mich gerne zur Verfügung.
Zum Aufbau des Dokumentes ist zu sagen, dass ich zunächst eine allgemeine Begriffserklärung durchführe und anschließend darstelle, wie ich versucht habe mein Projekt um zusetzten.
Um das Dokument möglichst kleinzuhalten, gehe ich nicht allzu ausführlich auf gelöste Problemstellung ein, diese konnte ich meistens durch Dokumentation
oder Videos/ Tutorials beseitigen. Anschließend noch anzumerken, da ich innerhalb einer Ubuntu VM gearbeitet habe, wäre es bei Bedarf möglich auch die Virtual Machine zur Verfügung zustellen.


## DevOps:

Development und Operationen. Beschreibt ein Konzept, welches kontinuierliche Bereitstellung von Software ermöglicht. Dies ermöglicht schon im Entwicklungsstadium erste Test-Szenarien.
Ein DevOps Team entwickelt und betreibt die Software. Es gibt also keine Trennung mehr zwischen Betriebs- und Entwicklungsteams, wie vormals Standard.
![](assets/Devops.jpeg)

## Grundbegriffe:

### Git

Git ist das gängigste ein Tool, um Versionierung, Verteilung und Deployment von Code durchzuführen.

### Docker

Docker wird benutzt, um Software unabhängig der Hardware zu emulieren.

### Gitlab

Gitlab ist ein Anbieter, um (im Normalfall) ein über Internet erreichbares Git Repository zu hosten.

### AWS Amplify

Ein AWS Dienst ähnlich wie Googles Firebase, welcher ermöglicht Kontinuierlich eine Full Stack Webanwendung zu hosten.
Um kurz noch zu Vergleichen, Firebase wurde 2012 eingeführt, Amplify 2018. Meiner oberflächlichen Beobachtung nach würde ich sagen das
Firebase mehr verbreitet/ bekannt benutzt ist.

### Go
Go oder Golang ist eine von Google und den Entwicklern der Programmiersprache C entwickelte Programmiersprache.

### Flutter
Ein auf Dart basierendes Framework zum Entwickeln von Frontend Applications. Ebenfalls von Google entwickelt/ gefördert.

## Projekt Overview:

Meine Aufgabenstellung war darin, ein Projekt unter Anwendung von DevOps Konzepten kontinuierlich über AWS zu hosten. Da meine Idee von Anfang an war eine Webanwendung zu kreieren.
Ich entschied mich aufgrund von Convenience Faktoren dafür AWS Amplify zu verwenden. Es wäre auch möglich gewesen mithilfe von AWS CodePipline bzw. Codebuild eine ähnliche Funktionalität
bereitzustellen, jedoch wäre dann ein extra Service der den fertigen Build hostet nötig gewesen. AWS Amplify baut und stellt dagegen die Ressourcen selbständig bereit.
Auch erstellt AWS Amplify automatisch Ressourcen, die für die kontinuierliche Bereitstellung vonnöten sind (bspw.: Build Settings), während man bei CodePipline diese einzeln konfigurieren müsste.
Die Funktion des Programms war folgendermaßen gedacht:

![AwsSeminararbeit_LH.png](assets/AwsSeminararbeit_LH.png)

### Schritte zum Reproduzieren

Mein erster Schritt war es, ein Gitlab Repository anzulegen. Die Entscheidung für GitLab viel deshalb, weil ich damit schon gearbeitet hatte, man hätte auch ohne weiteres
den AWS CodeCommit oder ähnliche Services benützen können. Ich probierte erstmal ein Programm in Go zu schreiben, das einfach nur eine einfache Response zurückgibt ohne ein
Template oder ähnliches zu rendern. Dort scheiterte ich aber daran, dass mein Backend nicht erkannt wurde. Ich entschied mich deshalb erstmal ein Frontend zubauen und zu hosten.
Ich entschied mich ein für mich neues Framework zum Entwickeln von Frontends zu testen, da ich mit anderen vorher benutzten nur teilweise zurechtkam.
Da ich gesehen hatte, dass Flutter von Amplify unterstützt wird, baute ich mit Flutter eine Oberfläche.
Anschließend war die Verbindung zwischen GitLab und Amplify zu erstellen. Hier wählte ich den »Host WepApp« Modus. Man wird daraufhin aufgefordert, einen Provider anzugeben.
Man autorisiert anschließend noch AWS den Zugriff zu Gitlab und kann dann den Prozess fertigstellen. Zu beachten war bei mir noch, dass ich einen Pfad in der Zeile »Monorepo« angeben musste.
Probleme hatte ich damit, das ich nicht verstanden habe, welche Funktonalität das »Pubspec.yml« innehat. Ich versuchte zunächst eigenständig das auszufüllen, jedoch fand ich ein Video wo gezeigt wurde, dass dort keine Einstellungen
vorzunehmen sind, sondern Amplify diese automatisch vornimmt.

![img.png](assets/img.png)

Anschließend wollte ich noch ein Backend initialisieren, hierfür erstellte ich mithilfe der Amplify CLI eine Lambdafunktion, ich schrieb diese in Go.
Ich erstellte außerdem ebenfalls mit der Amplify CLI on Top der Lambdafunktion die Ressource API Gateway. Ich konnte lokal die Funktion testen starten jedoch nach Push innerhalb des Browsers nicht die Funktion aufrufen.
Mithilfe von Herrn Karnehm konnte ich letzte Woche feststellen, dass Go Lambda Image nicht mehr die neuste Version GO Version unterstützt und noch
dieses Jahr entfernt wird. Ich entwickelte deshalb daheim ein Docker Image, welches die geforderte Funktonalität erfüllt. Ich benutze hierbei ein Dockerimage der ECR Public Gallery.

![img_1.png](assets/img_1.png)

Anschließend hab ich das Dockerbuild File in die AWS ECR (Elastic Container Registry) gepusht. Ich erstellte anschließend mit dem Dockerimage eine Lambdafunktion (siehe Lambda: LH_Seminararbeit).
Durch Zufall fand ich anschließend einen Workaround, um doch meinen Code mithilfe von dem Go1.X Image von Lambda auch meinen Code auszuführen. Anscheinend wurde das von mir ursprünglich benutzte Package
mux zum Erstellen eines ApiEndpoints nicht mehr von AWS
unterstützt, jedoch ein anderes Package gefunden auf dem offiziellen Github Repository von AWS mit ähnlicher Funktion und Syntax schon. Ich implementierte auch diese Lambdafunktion anfangs nur Test weiße jedoch benutze ich danach
nur noch diese Lambdafunktion, da ich mir dachte eine von AWS bereitgestellte Bibliothek wird bestimmt besser sein als mein selbstgeschriebener Code innerhalb eines Containers.
Um die Lambdafunktion erreichen zu können musste ich noch die AWS Ressource API Gateway und die Endpunkte konfigurieren.

![img_2.png](assets/img_2.png)

Beispiel Response:


![img_3.png](assets/img_3.png)

Ich versuchte anschließend eine Website zubauen, welche oben gezeigten Responses verarbeitet und darstellt. Jedoch konnte ich das in Flutter aufgrund von unzureichenden Kenntnissen nicht realisieren.
Hier war das Problem, dass der Http Service von Flutter nur sehr eingeschränkt CORS handeln kann. Auch nach dem ich in den Security-Settings von Flutter einige Einstellungen aufgehoben hatte, konnte ich nicht auf das JSON zugreifen.
Hier anzumerken ich bin mir relativ sicher die CORS Einstellung des Gateways auf alle Quellen gesetzt zuhaben. Jedoch bin ich mir nicht sicher, da ich keine Veränderungen nach dem Deployen in irgendeiner Einstellung innerhalb der neuen API-Gateway-Konsole beobachten konnte.
Die Dokumentation von AWS ist meiner Meinung nach zu diesem Thema gelinde gesagt schwammig.

Alle Ressourcen, die ich benutze, auch die CSV Datei, aus der die Fragen entnommen wurden befinden, sich in einem S3 Bucket mit dem Namen: »kdfjkfdlfjdkfslfjsaklfjlajf«. Der Name ist einfach nur wegen der Vorgabe das der Name eines Buckets weltweit einzigartig sein muss entstanden.
Dort liegen auch, die Bilder die auf der Website dargestellt werden. Der S3 Bucket ist öffentlich erreichbar, damit sich die Website die Ressourcen übers Internet holen kann. Jedoch ist nur »Public Read« gesetzt, somit kann man nicht aus dem Internet
Ressourcen löschen. In produktiven Systeme würde ich über Amplify einen privaten Bucket generieren lassen. Jedoch wollte ich zum Testen, innerhalb meiner Seminararbeit, möglichst schon Funktonalität wie darstellen von Bildern lokal getestet haben.

### Informationen zu geschriebenem Code
#### Flutter
Da ich kurzfristig einige für mich nicht mehr umsetzbare Ideen verworfen hatte, hab ich einiges an Flutter Code verworfen, diesen könnte ich bei Bedarf noch zur Verfügung stellen.
Die erstellte Homepage befindet sich in home_view.dart gebaut wird das Widget mithilfe einer Navigationsleiste (navigation_bar.dart) und »Container« in welchen Bilder dargestellt werden (home_continer.dart). CenteredView() sorgt dafür das alle Children eine gewisse Größe nicht
Überschreiten und Padding zum Rand vorhanden ist. Im Build folder befindet sich die kompilierten Dateien während sich im Lib Folder die Vorlagen in Dart befinden.

#### Go
Das geschriebene Gofile main.go beinhaltet alles an Code, während go.mod und go.sum die Dependencies beinhalten. Dokumentation des Codes befindet sich im Code selber.


![golang.png](assets/golang.png)

## Fazit:

Leider konnte ich jetzt am Ende aufgrund von Zeitdruck mein Projekt nicht komplett abschließen. Auch wenn mich die Seminararbeit einige Nerven (vor allem durch Amplify 😠) gekostet hat, hab ich trotzdem viel lernen können.
AWS wird sicherlich noch einige Jahre existieren, deshalb bin ich mir sicher, dass mich diese erworbenen Kenntnisse weiterbringen. Was mich allerdings bisschen stört, ist das die Dokumentation teilweise unvollständig oder nicht vorhanden ist.
Von einem Unternehmen in der Größe erwarte ich eigentlich, dass wenn ein Link auf eine Ressource zeigt, diese auch existiert. Auch finde ich teilweise die AWS Management Console sehr komisch beziehungsweise schwer zu verstehen. Gefühlt musste man sich in jedes neue
Modul erstmal eine halbe Stunde einarbeiten um sich in der Oberfläche zurechtzufinden. Abschließend will ich mich noch bedanken, dass es auch kurzfristig möglich war das WPM anzubieten, es war mit das beste das ich besuchen durfte. 
