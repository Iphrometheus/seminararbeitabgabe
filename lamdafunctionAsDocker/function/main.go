package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"math/rand"
	"net/http"
	"os"
	"slices"
	"sort"
	"strconv"
	"strings"
)

// ReadCsvFromUrl Function gets CSV file from specified Url and reads them with
// the Csv reader Package return: data as type two arrays of string
func ReadCsvFromUrl() [][]string {
	resp, err := http.Get("https://kdfjkfdlfjdkfslfjsaklfjlajf.s3.eu-central-1.amazonaws.com/questions.csv")
	if err != nil {
		exit("couldn't get the csv")
	}
	defer resp.Body.Close()
	reader := csv.NewReader(resp.Body)
	reader.Comma = ';'
	data, err := reader.ReadAll()
	if err != nil {
		exit("Failed to read")
	}
	return data
}

// make_Json converts a map of strings into a json serialize array of bytes return: []byte
func make_Json(myMap map[string]string) []byte {
	var jsonString []byte = nil
	var err error = nil
	jsonString, err = json.MarshalIndent(myMap, "", "    ")
	if err != nil {
		exit("Json failed")
	}
	return jsonString
}

// writes the Output to an Interface in case of an error a custom exit condition will be triggered
func sendQuiz(w http.ResponseWriter, r *http.Request) {
	quests := createQuestions()
	response := make_Json(quests)
	w.Header().Set("Content-Type", "application/json")
	_, err := fmt.Fprintf(w, string(response))
	if err != nil {
		exit("Http Response doesn't work")
	}
}

// generates a Webserver with the Mux package handels API calls on the specified
// endpoints with the specified Methods
func main() {
	router := mux.NewRouter()
	router.HandleFunc("/getQuestions", sendQuiz).Methods("GET")
	router.HandleFunc("/results", handleResults).Methods("POST")
	http.ListenAndServe(":80", router)
}

// create Questions gets 15 random Questions from the ReadCsvFrom Url() Function and generates a map of type [string]string
// return: map[string]string
func createQuestions() map[string]string {
	ints := []int{}
	for i := 0; i < 15; i++ {
		tmp := rand.Intn(30-1) + 1
		for {
			if slices.Contains(ints, tmp) {
				tmp = rand.Intn(30-1) + 1
			} else {
				break
			}
		}
		ints = append(ints, tmp)
	}
	sort.Ints(ints)
	lines := ReadCsvFromUrl()
	tmp := [][]string{}
	for x := range ints {
		t := ints[x]
		tmp = append(tmp, lines[t-1])
	}
	questions := parseLines(tmp)
	placeholder := []string{}
	for _, p := range questions {
		placeholder = append(placeholder, p.q)
	}
	ret := makeString(placeholder, ints)
	return ret
}

// makes the final string to return for createQuestions
func makeString(lines []string, zahl []int) map[string]string {
	ret := make(map[string]string)
	for i := range lines {
		ret[strconv.Itoa(zahl[i])] = lines[i]
	}
	return ret
}

// function to Separate question and answers reads the incoming array into the quest struct
func parseLines(lines [][]string) []quest {
	ret := make([]quest, len(lines))
	for i, line := range lines {
		ret[i] = quest{
			q: line[0],
			a: line[1],
		}
	}
	return ret
}

// struct for the parseLines funktion
type quest struct {
	q string
	a string
}

// Custom Function fore error Handling
func exit(msg string) {
	fmt.Println(msg)
	os.Exit(-1)
}

// replace does replace every space and make every string into lower case
func replace(str string) string {
	strings.ReplaceAll(str, " ", "")
	strings.ToLower(str)
	return str
}

// checks the incoming strings if identically return 1 else 0
func comp(str1 string, str2 string) int {
	if str1 == str2 {
		return 1
	}
	return 0
}

// Function to get and decode the incoming answers and to calculate with comp the result
// writes the result to the rw ResponseWriter
func handleResults(rw http.ResponseWriter, r *http.Request) {
	var data map[string]string
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		exit("JSON Conversion Failed")
	}
	lines := ReadCsvFromUrl()
	if err != nil {
		exit("Error Failed to parse the provided CSV file")
	}
	res := 0
	for s, i := range data {
		i = replace(i)
		sToInt, _ := strconv.Atoi(s)
		tmp := ""
		tmp = lines[sToInt][1]
		tmp = replace(tmp)
		res += comp(i, tmp)
	}
	answers := map[string]int{"answers": res}
	ret, fail := json.Marshal(answers)
	if fail != nil {
		exit(fmt.Sprintf("Failed to create json"))
	}
	rw.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(rw, string(ret))
}
