import 'package:flutter/material.dart';
import 'package:frontend/widgets/HomeContainer/home_container.dart';
import 'package:frontend/widgets/centered__view/centered_view.dart';
import 'package:frontend/widgets/navigation_bar/navigation_bar.dart';

class HomeView extends StatelessWidget {
  const HomeView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 181, 231, 207),
      body: CenteredView(
          child: Column(children: [
        Navigation_Bar(),
        Expanded(
          child: Row(
            children: [
              HomeContainer(
                  url:
                      "https://kdfjkfdlfjdkfslfjsaklfjlajf.s3.eu-central-1.amazonaws.com/aws-amplify.jpg"),
              SizedBox(
                width: 150,
              ),
              HomeContainer(
                  url:
                      "https://kdfjkfdlfjdkfslfjsaklfjlajf.s3.eu-central-1.amazonaws.com/golang.png"),
              SizedBox(
                width: 150,
              ),
              HomeContainer(
                  url:
                      "https://kdfjkfdlfjdkfslfjsaklfjlajf.s3.eu-central-1.amazonaws.com/flutter_logo.png")
            ],
          ),
        )
      ])),
    );
  }
}
