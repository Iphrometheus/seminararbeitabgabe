import 'package:flutter/material.dart';
import 'package:frontend/download/file_download.dart';
import 'package:frontend/views/home/home_view.dart';

class Navigation_Bar extends StatelessWidget {
  const Navigation_Bar({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 100,
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          SizedBox(
            height: 80,
            width: 150,
            child: Image.network(
                "https://kdfjkfdlfjdkfslfjsaklfjlajf.s3.eu-central-1.amazonaws.com/flutter_aws_logo.png"),
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              _NavbarItem("Home", (_) => HomeView()),
              SizedBox(
                width: 60,
              ),
              SizedBox(
                width: 60,
              ),
            ],
          )
        ]));
  }
}

class _NavbarItem extends StatelessWidget {
  final String title;
  final WidgetBuilder routeBuilder;
  const _NavbarItem(this.title, this.routeBuilder);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        Navigator.push(context, MaterialPageRoute(builder: routeBuilder));
      },
      child: Text(
        title,
        style: const TextStyle(fontSize: 18),
      ),
    );
  }
}
