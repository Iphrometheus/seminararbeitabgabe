import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';

//funktoniert nicht und ist auch nicht eingebunden
class FileDownloadButton extends StatelessWidget {
  final String fileUrl =
      "https://kdfjkfdlfjdkfslfjsaklfjlajf.s3.eu-central-1.amazonaws.com/README.pdf";
  final String fileName;

  FileDownloadButton({required this.fileName});

  Future<void> downloadFile() async {
    final response = await http.get(Uri.parse(fileUrl));

    if (response.statusCode == 200) {
      final directory = await getApplicationDocumentsDirectory();
      final file = File('${directory.path}/$fileName');

      await file.writeAsBytes(response.bodyBytes);
      print('Datei heruntergeladen: ${file.path}');
    } else {
      throw Exception('Fehler beim Herunterladen der Datei');
    }
  }

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: downloadFile,
      child: Text('Datei herunterladen'),
    );
  }
}
