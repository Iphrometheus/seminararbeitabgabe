package helpers

import "os"

// checks if the Enviorment variable LAMBDA_TASK_ROOT is set
// on set return true else false
func IsLambda() bool {
	if lambdaTaskRoot := os.Getenv("LAMBDA_TASK_ROOT"); lambdaTaskRoot != "" {
		return true
	}
	return false
}
