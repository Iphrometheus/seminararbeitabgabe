package main

import (
	"context"
	"encoding/csv"
	"example.com/m/v2/helpers"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	fiberadapter "github.com/awslabs/aws-lambda-go-api-proxy/fiber"
	"github.com/gofiber/fiber/v2"
	"math/rand"
	"net/http"
	"os"
	"slices"
	"sort"
	"strconv"
	"strings"
)

var fiberLambda *fiberadapter.FiberLambda

// ReadCsvFromUrl Function gets CSV file from specified Url and reads them with
// the Csv reader Package return: data as type two arrays of string
func readCsvFromUrl() [][]string {
	resp, err := http.Get("https://kdfjkfdlfjdkfslfjsaklfjlajf.s3.eu-central-1.amazonaws.com/questions.csv")
	if err != nil {
		exit("couldn't get the csv")
	}
	defer resp.Body.Close()
	reader := csv.NewReader(resp.Body)
	data, err := reader.ReadAll()
	if err != nil {
		exit("Failed to read")
	}
	return data
}

// Handler creates an Handler for AWS Lambda
func Handler(ctx context.Context, request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	return fiberLambda.ProxyWithContext(ctx, request)
}

// writes the Output to the Endpoint
func sendQuiz() map[string]string {
	quests := createQuestions()
	return quests
}

// generates a Webserver and calls the functions on Request. Use the Fiber and the Lambda Fiber package Function. The isLambda() define in helpers/helpers.go
func main() {
	router := fiber.New(fiber.Config{
		DisableKeepalive: true,
	})
	router.Get("/", func(ctx *fiber.Ctx) error {
		return ctx.JSON(sendQuiz())
	})
	router.Post("/results", func(ctx *fiber.Ctx) error {
		return ctx.JSON(handleResults(ctx))
	})
	router.Get("/LH_Seminararbeitv1", func(ctx *fiber.Ctx) error {
		return ctx.SendString("HelloWorld")
	})
	if helpers.IsLambda() {
		fiberLambda = fiberadapter.New(router)
		lambda.Start(Handler)
	} else {
		router.Listen(":8080")
	}
}

// create Questions gets 15 random Questions from the ReadCsvFrom Url() Function and generates a map of type [string]string
// return: map[string]string
func createQuestions() map[string]string {
	ints := []int{}
	for i := 0; i < 15; i++ {
		tmp := rand.Intn(30-1) + 1
		for {
			if slices.Contains(ints, tmp) {
				tmp = rand.Intn(30-1) + 1
			} else {
				break
			}
		}
		ints = append(ints, tmp)
	}
	sort.Ints(ints)
	lines := readCsvFromUrl()
	tmp := [][]string{}
	for x := range ints {
		t := ints[x]
		tmp = append(tmp, lines[t-1])
	}
	questions := parseLines(tmp)
	placeholder := []string{}
	for _, p := range questions {
		placeholder = append(placeholder, p.q)
	}
	ret := makeString(placeholder, ints)
	return ret
}

// makes the final string to return for createQuestions
func makeString(lines []string, zahl []int) map[string]string {
	ret := make(map[string]string)
	for i := range lines {
		ret[strconv.Itoa(zahl[i])] = lines[i]
	}
	return ret
}

// function to Separate question and answers reads the incoming array into the quest struct
func parseLines(lines [][]string) []quest {
	ret := make([]quest, len(lines))
	for i, line := range lines {
		ret[i] = quest{
			q: line[0],
			a: line[1],
		}
	}
	return ret
}

// struct for the parseLines funktion
type quest struct {
	q string
	a string
}

// Custom Function fore error Handling
func exit(msg string) {
	fmt.Println(msg)
	os.Exit(-1)
}

// replace does replace every space and make every string into lower case
func replace(str string) string {
	strings.ReplaceAll(str, " ", "")
	strings.ToLower(str)
	return str
}

// checks the incoming strings if identically return 1 else 0
func comp(str1 string, str2 string) int {
	if str1 == str2 {
		return 1
	}
	return 0
}

// Function to get and decode the incoming answers and to calculate with comp the result
// reads the input from ctx
func handleResults(ctx *fiber.Ctx) map[string]int {
	var data map[string]string
	ctx.BodyParser(&data)
	lines := readCsvFromUrl()
	res := 0
	for s, i := range data {
		i = replace(i)
		sToInt, _ := strconv.Atoi(s)
		sToInt -= 1
		tmp := ""
		tmp = lines[sToInt][1]
		tmp = replace(tmp)
		res += comp(i, tmp)
	}
	answers := map[string]int{"answers": res}
	return answers
}
